#include "catch.hpp"
#include "../src/search.hpp"

// Some basic test cases

TEST_CASE("Testing successful search","[succ]")
{
  CHECK(search("TTAGAAGGCTTAGAGGTTAC", "TTAG") == 0);
  CHECK(search("TTACAAGGCTTAGAGGTTAG", "TTAG") == 9);
  CHECK(search("TTACAAGGCTTACAGGTTAG", "TTAG") == 16);
}


TEST_CASE("Testing unsuccessful searches", "[notsucc]")
{
  CHECK(search("TTACAAGGCTTAGAGGTTAG", "UCTA") == -1);
  CHECK(search("TTACAAGGCTTAGAGGTTAG", "ACTA") == -1);
  CHECK(search("ATTACAAGGCTTAGAGGTTC", "TTCA") == -1);
}


TEST_CASE("Testing edge cases","[strange]")
{
 CHECK(search("You were sent this email because you chose to receive updates from Caribbean Broadcasting Corporation. If you don't want these updates anymore, you can unsubscribe here", "road") == 78);

  CHECK(search("ATTACAAGGCTTAGAGGTTC", "") == -1);
  CHECK(search("", "ATCG") == -1);
  CHECK(search("", "") == -1);
 
}

